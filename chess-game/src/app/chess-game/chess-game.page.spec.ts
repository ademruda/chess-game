import { async, ComponentFixture, TestBed } from '@angular/core/testing';
import { IonicModule } from '@ionic/angular';

import { ChessGamePage} from './chess-game.page';

describe('HomePage', () => {
  let component: ChessGamePage;
  let fixture: ComponentFixture<ChessGamePage>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChessGamePage],
      imports: [IonicModule.forRoot()]
    }).compileComponents();

    fixture = TestBed.createComponent(ChessGamePage);
    component = fixture.componentInstance;
    fixture.detectChanges();
  }));

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
