import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ChessGamePage } from './chess-game.page';

const routes: Routes = [
  {
    path: '',
    component: ChessGamePage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ChessGamePageRoutingModule {}
